import Foundation

//Создайте по 2 enum с разным типом RawValue
enum Weather: String {
    case sunny = "Sunny"
    case rainy = "Rainy"
}
enum Days: Int {
    case monday = 1
    case tuesday = 2
}

//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
enum Gender {
    case male
    case female
}
enum AgeCategory {
    case youth 
    case youngAdult 
    case adult 
    case senior 
}
enum Experience {
    case beginner 
    case intermediate 
    case advanced 
    case expert 
}

//Создать enum со всеми цветами радуги
enum RainbowColor {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

//Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
enum FruitColor {
    case appleGreen
    case bananaYellow
    case cherryRed
}
let fruitColors: [FruitColor] = [.appleGreen, .bananaYellow, .cherryRed]
func printEnumCases(cases: [FruitColor]) {
    for fruitColor in cases {
        switch fruitColor {
        case .appleGreen:
            print("apple green")
        case .bananaYellow:
            print("banana yellow")
        case .cherryRed:
            print("cherry red")
        }
    }
}
printEnumCases(cases: fruitColors)

//Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки
enum Score: String {
    case excellent = "Отлично"
    case good = "Хорошо"
    case satisfactory = "Удовлетворительно"
    case unsatisfactory = "Неудовлетворительно"
}
func assignGrade(score: Score) -> Int {
    switch score {
    case .excellent:
        return 5
    case .good:
        return 4
    case .satisfactory:
        return 3
    case .unsatisfactory:
        return 2
    }
}
let studentScore = Score.good
let studentGrade = assignGrade(score: studentScore)
print("Студент получил оценку: \(studentGrade)")

//Создать метод, который выводит в консоль какие автомобили стоят в гараже, используйте enum
enum CarBrand: String {
    case toyota
    case ford
    case bmw
    case audi
}
struct Garage {
    var cars: [CarBrand]
    
    func printCars() {
        for car in cars {
            switch car {
            case .toyota:
                print("Toyota стоит в гараже.")
            case .ford:
                print("Ford стоит в гараже.")
            case .bmw:
                print("BMW стоит в гараже.")
            case .audi:
                print("Audi стоит в гараже.")
            }
        }
    }
}
let myGarage = Garage(cars: [.toyota, .audi, .bmw])
myGarage.printCars()

